﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthUIController : MonoBehaviour {
    public Slider healthSlider;
    public int hp;
    public int maxHp;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        healthSlider.GetComponent<Slider>().value = (float)hp / (float)maxHp;

    }
}
