﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class PlayerDataController : MonoBehaviour {
    public PlayerListData PlayerListData;
    public ArrayList PlayerList;
    public PlayerData CurrentPlayer;
    public UnityEngine.UI.Text PlayerNameText;

    public GameObject FemaleAvatar;
    public GameObject MaleAvatar;

     Component[] FemaleSkinMeshs;
     Component[] FemaleSkinMeshs2;
     Component[] MaleSkinMeshs;
    // Use this for initialization
    void Start () {

        FemaleSkinMeshs = FemaleAvatar.GetComponentsInChildren<SkinnedMeshRenderer>();
        FemaleSkinMeshs2 = FemaleAvatar.GetComponentsInChildren<MeshRenderer>();
        MaleSkinMeshs = MaleAvatar.GetComponentsInChildren<SkinnedMeshRenderer>();
        Load();

        CurrentPlayer = (PlayerData)PlayerList[0];
        PlayerNameText.text = CurrentPlayer.name;
        CheckGender();
    }

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfor.dat");
        PlayerListData.playerList = PlayerList;
        bf.Serialize(file, PlayerListData);
        file.Close();

        Load();
    }

    public void Load()
    {
        Debug.Log(Application.persistentDataPath);
        if(File.Exists(Application.persistentDataPath + "/playerInfor.dat"))
        {
            Debug.Log(Application.persistentDataPath);
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfor.dat", FileMode.Open);
            PlayerListData data = (PlayerListData)bf.Deserialize(file);
            file.Close();

            PlayerList = data.playerList;
        }
        else
        {
            PlayerListData = new PlayerListData();
            PlayerList = new ArrayList();
            foreach (SkinnedMeshRenderer renderer in MaleSkinMeshs)
            {
                renderer.enabled = false;
            }
            foreach (SkinnedMeshRenderer renderer in FemaleSkinMeshs)
            {
                renderer.enabled = false;
            }

            foreach (MeshRenderer renderer in FemaleSkinMeshs2)
            {
                renderer.enabled = false;
            }
        }
    }

    public void AddPlayer(PlayerData player)
    {
        PlayerList.Add(player);
        Save();
    }
    public void ChangePlayer(PlayerData player)
    {
        CurrentPlayer = player;
        PlayerNameText.text = CurrentPlayer.name;
        CheckGender();
    }

    public void CheckGender()
    {
        if (CurrentPlayer.gender == true)
        {
            foreach (SkinnedMeshRenderer renderer in MaleSkinMeshs)
            {
                renderer.enabled = false;
            }
            foreach (SkinnedMeshRenderer renderer in FemaleSkinMeshs)
            {
                renderer.enabled = true;
            }

            foreach (MeshRenderer renderer in FemaleSkinMeshs2)
            {
                renderer.enabled = true;
            }
            
        }
        else
        {
            foreach (SkinnedMeshRenderer renderer in FemaleSkinMeshs)
            {
                renderer.enabled = false;
            }

            foreach (MeshRenderer renderer in FemaleSkinMeshs2)
            {
                renderer.enabled = false;
            }
            foreach (SkinnedMeshRenderer renderer in MaleSkinMeshs)
            {
                renderer.enabled = true;
            }
        }
    }
}

[Serializable]
public class PlayerListData
{
    public ArrayList playerList;
}

[Serializable]
public class PlayerData
{
    public string name;
    public int age;
    public float weight;
    public bool gender;

}
