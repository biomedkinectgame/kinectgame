﻿public class Tags
{
    public const string Player = "Player";
    public const string Terrain = "Terrain";
    public const string Enemy = "Enemy";
}

public class Scenes
{
    public const string TitleScreen = "Title_Screen";
    public const string Trans1 = "Trans-v1";
    public const string Trans2 = "Trans-v2";
    public const string Trans3 = "Trans-v3";
    public const string TransIntro = "Trans_intro";
    public const string CastleStorm = "Castle Storm";
    public const string CastleStorm2 = "Castle Storm 2";
    public const string VillageDefense = "Village Defense";
    public const string VillageDefense2 = "Village Defense 2";
    public const string RiverRecovery = "River Recovery";
    public const string Sharpshooter = "Sharpshooter";
    public const string SnowEscape = "Snow Escape";
    public const string WarmUp = "WarmUp";
}