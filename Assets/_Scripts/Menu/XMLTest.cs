﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class XMLTest : MonoBehaviour {

    public UnityEngine.UI.Text PlayerName;
    XMLContainer container2;
    XMLContainer LoadContainer;
    public UnityEngine.UI.InputField NameInputField;
    public UnityEngine.UI.InputField AgeInputField;
    string Name;
    string Age;

    // Use this for initialization
    void Start () {
        LoadContainer = new XMLContainer();
        container2 = new XMLContainer();
        container2.Players = new List<Player>();
        Load();
    }
	
    void Update()
    {

        Name = NameInputField.text;
        Age = AgeInputField.text;
    }

    public void ConfirmInput()
    {
        Add();
        Save();
    }

    public void Load()
    {
        container2 = XMLContainer.Load(Path.Combine(Application.dataPath, "PlayerData.xml"));


        PlayerName.text = container2.Players.Count.ToString();
    }

    public void Save()
    {
        container2.Save(Path.Combine(Application.dataPath, "PlayerData.xml"));
    }

    public void Add()
    {
        Player playerdata = new Player();
        playerdata.name = Name;
        playerdata.age= Age;

        container2.Players.Add(playerdata);
    }
}
