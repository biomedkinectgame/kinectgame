﻿using UnityEngine;
using System.Collections;

public class FlashScreenController : MonoBehaviour {
    public GameObject LeftHand;
    public GameObject RightHand;
    public GameObject Head;
    public GameObject Camera;
	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
        if (LeftHand.transform.position.y > Head.transform.position.y && RightHand.transform.position.y > Head.transform.position.y)
        {
            Camera.transform.position = new Vector3(89.5f, 1, -7);
            Destroy(gameObject);
        }
	
	}
}
