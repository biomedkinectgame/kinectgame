﻿using UnityEngine;
using System.Collections;

public class RightHand : MonoBehaviour {
    public bool Collide;
	// Use this for initialization
	void Start () {
        Collide = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Cube")
        {
            Collide = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Cube")
        {
            Collide = false;
        }
    }
}
