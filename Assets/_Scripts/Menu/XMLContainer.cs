﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("PlayerList")]
public class XMLContainer
{

    [XmlArray("Players"), XmlArrayItem("Player")]
    public List<Player> Players = new List<Player>();

    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(XMLContainer));
        using (var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);

            stream.Close();
        }
    }

    



public static XMLContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(XMLContainer));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            stream.Close();
            return serializer.Deserialize(stream) as XMLContainer;
        }
    }
}
