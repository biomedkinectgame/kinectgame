﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;

public class XMLController : MonoBehaviour {
    string fileLocation, fileName;
    string PlayerData;
    UserData myData;
    string data;

    StreamWriter writer;
    FileInfo t;


    // Use this for initialization
    void Start () {
        fileLocation = Application.dataPath;
        fileName = "PlayerData.xml";

        PlayerData = "Player";


        t = new FileInfo(fileLocation + "\\" + fileName);
        writer = t.CreateText();
        myData = new UserData();
    }
	
	// Update is called once per frame
	void Update () {
    }

    public void writeName(string inputName, string inputAge)
    {
        myData.playerData.name = inputName;
        myData.playerData.age = inputAge;
        data = SerializeObject(myData);
        WrieteXML();
    }

    string UTF8ByteArrayToString(byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        string constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    public void inputConfirm()
    {
        data = SerializeObject(myData);
        WrieteXML();
    }

    void WrieteXML()
    {

        writer.Write(data);
        writer.Close();
    }


    string SerializeObject(object pObject)
    {
        string XmlizedString = null;
        MemoryStream memoryStream = new MemoryStream();
        XmlSerializer xs = new XmlSerializer(typeof(UserData));
        XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
        xs.Serialize(xmlTextWriter, pObject);
        memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
        return XmlizedString;
    }

    public class UserData
    {
        public PlayerData playerData;

        public struct PlayerData
        {
            public string name;
            public string age;
        }
    }
}
