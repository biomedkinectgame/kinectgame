﻿using UnityEngine;
using System.Collections;

public class NewPlayerController : MonoBehaviour {

    public UnityEngine.UI.InputField NameInputField;
    public UnityEngine.UI.InputField AgeInputField;
    public UnityEngine.UI.InputField WeightInputField;
    public UnityEngine.UI.Toggle MaleToggle;
    public UnityEngine.UI.Toggle FemaleToggle;
    public PlayerDataController playerDataController;

    public GameObject FemaleAvatar;
    public GameObject MaleAvatar;

    Component[] FemaleSkinMeshs;
    Component[] FemaleSkinMeshs2;
    Component[] MaleSkinMeshs;


    int age;
    string name;
    float weight;
    bool gender;

    // Use this for initialization
    void Start () {
        FemaleSkinMeshs = FemaleAvatar.GetComponentsInChildren<SkinnedMeshRenderer>();
        FemaleSkinMeshs2 = FemaleAvatar.GetComponentsInChildren<MeshRenderer>();
        MaleSkinMeshs = MaleAvatar.GetComponentsInChildren<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(MaleToggle.isOn==true){
            SelectMale();
        }
        if (FemaleToggle.isOn == true)
        {
            SelectFemale();
        }
    }
     void SelectFemale()
    {
        foreach (SkinnedMeshRenderer renderer in FemaleSkinMeshs)
        {
            renderer.enabled = true;
        }

        foreach (MeshRenderer renderer in FemaleSkinMeshs2)
        {
            renderer.enabled = true;
        }
        foreach (SkinnedMeshRenderer renderer in MaleSkinMeshs)
        {
            renderer.enabled = false;
        }
    }

    void SelectMale()
    {
        foreach (SkinnedMeshRenderer renderer in FemaleSkinMeshs)
        {
            renderer.enabled = false;
        }

        foreach (MeshRenderer renderer in FemaleSkinMeshs2)
        {
            renderer.enabled = false;
        }
        foreach (SkinnedMeshRenderer renderer in MaleSkinMeshs)
        {
            renderer.enabled = true;
        }
    }

    public void CreatNewPlayer()
    {
        name = NameInputField.text;
        age = int.Parse(AgeInputField.text);
        weight = float.Parse(WeightInputField.text);
        if (MaleToggle.isOn == true)
        {
            gender = false;
        }
        if (FemaleToggle.isOn == true)
        {
            gender = true;
        }
        
        PlayerData player = new PlayerData();

        player.age = age;
        player.name = name;
        player.gender = gender;
        player.weight = weight;

        playerDataController.CurrentPlayer = player;
        playerDataController.PlayerNameText.text = player.name;

        if (playerDataController.PlayerListData == null) {
            Debug.Log("newPlayer: null");
        }
        playerDataController.AddPlayer(player);
    }
}
