﻿using UnityEngine;
using System.Collections;

/**
 * <summary>
 * FireArrow manages the Bow and Arrows, both loaded in the bow and already fired from the bow, in the level "Sharpshooter."
 * </summary>
 */
public class FireArrow : MonoBehaviour {

	private ArcheryGameController _agc;

	//AUDIO
	[Tooltip("The SFX that is heard when an arrow is fired")]
	public AudioSource sfx_bowLaunch;

	//CONSTANT VALUES
	private const int 	TELEPORT 			= 100000;
	private const float	GRAVITY 			= 0.1f;
	private const float	ROTATION_LOOK_X 	= 2.0f;
	private const float	ROTATION_LOOK_Y 	= 1.0f;
	private const float	MIN_ROTATION 		= -5.0f;
	private const float	MAX_ROTATION 		= 5.0f;
	private const int 	LOOK_INCREMENTS 	= 5;

	[Tooltip("The Bow-and-Arrow prefab consisting of the bow, the \'loaded_arrow\', \'arrow_base\', and \'arrow_start\'")]
	public GameObject _prefab;

	[Tooltip("The Plyer object in the scene")]
	public GameObject _player;

	[Tooltip("The Arrow currently loaded in the bow")]
	public GameObject loaded_arrow;

	[Tooltip("A base copy of the Arrow to copy to create more arrows")]
	public GameObject arrow_base;

	[Tooltip("The starting transform of all arrows when they are loaded in the bow")]
	public Transform arrow_start;

	[Tooltip("The GameObject that becomes the parent of all fired arrows")]
	public GameObject flyingArrows;

	private ArrayList shot_arrows;
	private int counter;
	private bool isArrowLoaded;
	private int look_counter;

	//TIMER VARIABLES
	private float timer;
	private float reload_timer;

	[Tooltip("The rate at which a new arrow is loaded into the bow (in seconds)")]
	public float reload_rate;

	// Use this for initialization
	void Start () {

		if (_player == null) {
			_player = GameObject.FindGameObjectWithTag("Player");
		}

		_agc = GameObject.Find("CONTROLLERS").GetComponent<ArcheryGameController>();

		isArrowLoaded = true;
		timer = 0.0f;
		reload_timer = 0.0f;
		if (reload_rate < 1.0f || reload_rate > 5.0f) {
			reload_rate = 2.5f;
		}
		reload_timer += reload_rate;

		shot_arrows = new ArrayList ();
		counter = 1;

		if (flyingArrows == null) {
			flyingArrows = GameObject.Find ("Flying Arrows");
			if (flyingArrows == null) {
				flyingArrows = GameObject.Instantiate(new GameObject());
				flyingArrows.gameObject.name = "Flying Arrows";
			}
		}

		look_counter = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKeyDown (KeyCode.Space) == true && isArrowLoaded == true){
			shootArrow (loaded_arrow);
		}

		timer += Time.deltaTime;
		if (timer > reload_timer) {
			reload_timer += reload_rate;
			if (isArrowLoaded == false) {
				reloadArrow ();
			}
		}

		//This for loop iterates over all the current arrows fired
		for (int i = 0; i < shot_arrows.Count; i++) {
			GameObject arrow = (GameObject)shot_arrows [i];		//	Get the arrow game object
			if (arrow == null) {
				shot_arrows.Remove (arrow);
			} 
			else {
				Rigidbody rb = arrow.GetComponent<Rigidbody> ();	// 	Get the Rigidbody Component
				//rb.WakeUp();
				Vector3 force = arrow.transform.FindChild ("target").transform.forward;
				//print ("force: " + force);
				//force.z *= -1;
				force.y -= GRAVITY;
				rb.AddForce (force, ForceMode.Impulse);
			}
		}//for - shot_arrows[i]

		//Destroy Arrow objects that ae no longe valid
		removeArrows();
	
	}

	public void shootArrow(){
		if (loaded_arrow != null) {
			sfx_bowLaunch.Play();
			shootArrow (loaded_arrow);
		}
	}

	private void shootArrow(GameObject arrow){
		if (_agc.getIsPaused() == false) {
			loaded_arrow.transform.parent = null;
			isArrowLoaded = false;
			loaded_arrow = null;
			shot_arrows.Add(arrow);
		}
	}

	private void reloadArrow(){
		if (_agc.getIsPaused() == true)
			return;
		
		loaded_arrow = GameObject.Instantiate (arrow_base);
		loaded_arrow.gameObject.name = "Arrow (" + counter + ")";
			counter++;
		loaded_arrow.transform.parent = _prefab.transform;
		loaded_arrow.transform.position = Vector3.Lerp (loaded_arrow.transform.position, arrow_start.position, Time.deltaTime * TELEPORT);
		loaded_arrow.transform.rotation = Quaternion.Lerp (loaded_arrow.transform.rotation, arrow_start.rotation, Time.deltaTime * TELEPORT);
		isArrowLoaded = true;
	}

	private void removeArrows(){
		//This for loop iterates over all the current arrows fired
		for (int i = 0; i < shot_arrows.Count; i++) {
			GameObject arrow = (GameObject)shot_arrows [i];		//	Get the arrow game object
			if (arrow == null) {
				shot_arrows.Remove (arrow);
			} 
			else {
				if (arrow.transform.position.y <= 0.0f) {
					//print ("\"" + arrow.name + "\" has been destroyed (y pos)");
					GameObject.Destroy (arrow);
				}

				if (arrow.GetComponent<Col_arrow>().getHasMadeContact() == true){
					//print ("\"" + arrow.name + "\" has been destroyed (madeContact)");
					GameObject.Destroy (arrow);
				}
			}


		}//for - shot_arrows[i]
	}

	public void aimLeft(){
		if (_agc.getIsPaused() == true)
			return;
		_player.transform.Rotate (0, -ROTATION_LOOK_X, 0);
	}

	public void aimRight(){
		if (_agc.getIsPaused() == true)
			return;
		_player.transform.Rotate (0, ROTATION_LOOK_X, 0);
	}

	public void aimUp(){
		if (_agc.getIsPaused() == true)
			return;
		
		if (look_counter < LOOK_INCREMENTS){
			_prefab.transform.Rotate(0, 0, ROTATION_LOOK_Y);
			look_counter++;
		}
	}

	public void aimDown(){
		if (_agc.getIsPaused() == true)
			return;
		
		if (look_counter > -LOOK_INCREMENTS){
			_prefab.transform.Rotate(0, 0, -ROTATION_LOOK_Y);
			look_counter--;
		}
	}
}
