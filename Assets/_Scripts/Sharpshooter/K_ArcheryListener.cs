﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/**
 * <summary>
 * K_ArcheryListener manages the detection of Kinect gestures for the level "Sharpshooter"
 * </summary>
 */
public class K_ArcheryListener : MonoBehaviour, KinectGestures.GestureListenerInterface {

	[Tooltip("The Script Oject that controls gameplay of this level.  (Searched for in \"Start()\" if null)")]
	public ArcheryGameController _acg;

	[Tooltip("The Script Oject that controls the Bow and Arrows.  (Searched for in \"Start()\" if null)")]
	public FireArrow _fa;

	[Tooltip("UI Text object to allows debug info to appear on the screen")]
	public Text debug_text;

	// Use this for initialization
	void Start () {
		
		if (_acg == null) {
			_acg = GameObject.Find ("CONTROLLERS").GetComponent<ArcheryGameController> ();
		}
		if (_fa == null) {
			_fa = GameObject.Find ("Bow-and-Arrow").GetComponent<FireArrow> ();
		}
	}

	public void UserDetected(long userId, int userIndex){
		//KinectManager manager = KinectManager.Instance;
		//manager.DetectGesture(userId, KinectGestures.Gestures.AimLeft);
		//manager.DetectGesture(userId, KinectGestures.Gestures.AimRight);
		//manager.DetectGesture(userId, KinectGestures.Gestures.AimUp);
		//manager.DetectGesture(userId, KinectGestures.Gestures.AimDown);
		//manager.DetectGesture(userId, KinectGestures.Gestures.SetToFire);
		//manager.DetectGesture(userId, KinectGestures.Gestures.FireArrow);
		//manager.DetectGesture(userId, KinectGestures.Gestures.RaiseRightHand);
	}

	public void UserLost(long userId, int userIndex){}

	public void GestureInProgress(long userId, int userIndex, KinectGestures.Gestures gesture, float progress, KinectInterop.JointType joint, Vector3 screenPos){}

	public bool GestureCompleted(long userId, int userIndex, KinectGestures.Gestures gesture, KinectInterop.JointType joint, Vector3 screenPos){
		

		if (gesture == KinectGestures.Gestures.WaveOff) {
			_acg.setIsPaused();
			if (_acg.getIsPaused() == true)
				debug_text.text = "* --- PAUSED --- *";
			else
				debug_text.text = "";
		} 
		else{ 
			if (gesture == KinectGestures.Gestures.Jump) {
				_acg.cyclePlayerLocation ();
			}
			else{ 
				if (gesture == KinectGestures.Gestures.SetToFire) {	
					debug_text.text = "SET TO FIRE";
				}
				else{
					if (gesture == KinectGestures.Gestures.FireArrow) {	
						debug_text.text = "FIRE ARROW"; 
						_fa.shootArrow ();
					}
					else {
						if (gesture == KinectGestures.Gestures.AimLeft) {
							debug_text.text = "AIM LEFT"; 
							_fa.aimLeft ();
						}
						else {
							if (gesture == KinectGestures.Gestures.AimRight) {
								debug_text.text = "AIM RIGHT";
								_fa.aimRight ();
							}
							else {
								if (gesture == KinectGestures.Gestures.AimUp) {
									debug_text.text = "AIM UP";
									_fa.aimUp();
								}
								else {
									if (gesture == KinectGestures.Gestures.AimDown) {
										debug_text.text = "AIM DOWN";
										_fa.aimDown();
									}
									else {
										//DO NOTHING
										debug_text.text = "------";
		}	}	}	}	}	}	}	}
			
		return true;
	}

	public bool GestureCancelled(long userId, int userIndex, KinectGestures.Gestures gesture, KinectInterop.JointType joint){
		return true;
	}

}
