﻿using UnityEngine;
using System.Collections;

/**
 * <summary>
 * ArcheryEnemyController manages the creation, pathfinding, destruction, and removal of the enemies for the level "Sharpshooter," 
 * including spawning enemies during waves, starting and stopping waves, and pathfinding for enemies.  The health of enemies is
 * tracked separately by "Col_enemy," but the health is monitored by this class.
 * </summary>
 */
public class ArcheryEnemyController : MonoBehaviour {

	[Tooltip("The GameObject that contains the scripts that control the game")]
	public GameObject CONTROLLERS;
	private ArcheryGameController _agc;
	private HudController _hud;

	//CONSTANT VALUES
	private const int TELEPORT 			= 100000;
	private const int MAX_ENEMIES 		= 50;
	private const int SAFETY_MARGIN 	= 15;
	private const int SPEED_LOW 		= 5;
	private const int SPEED_HIGH 		= 20;
	private const int ACC_LOW 			= 20;
	private const int ACC_HIGH 			= 100;
	private const float Y_OFFSET 		= 10.0f;

	//TIMER VARIABLES
	private float timer;
	private float wave_timer;

	[Tooltip("The amount of time (in seconds) between spawned waves of enemies")]
	public float spawn_rate;

	//PUBLIC VARIABLES
	[Tooltip("The base GameObject for the 1st type of enemy")]
	public GameObject enemyType_1;

	[Tooltip("The base GameObject for the 2nd type of enemy")]
	public GameObject enemyType_2;

	[Tooltip("The base GameObject for the 3rd type of enemy")]
	public GameObject enemyType_3;


	[Tooltip("A transform of a target for the enemies to \'attack\'")]
	public Transform dest_1;

	[Tooltip("A transform of a target for the enemies to \'attack\'")]
	public Transform dest_2;

	[Tooltip("A transform of a target for the enemies to \'attack\'")]
	public Transform dest_3;

	[Tooltip("A transform of a target for the enemies to \'attack\'")]
	public Transform dest_4;

	[Tooltip("A transform of a target for the enemies to \'attack\'")]
	public Transform dest_5;

	[Tooltip("A transform of a target for the enemies to \'attack\'")]
	public Transform dest_6;

	[Tooltip("A transform of a target for the enemies to \'attack\'")]
	public Transform dest_7;

	[Tooltip("A transform of a target for the enemies to \'attack\'")]
	public Transform dest_8;

	[Tooltip("A transform of a target for the enemies to \'attack\'")]
	public Transform dest_9;

	[Tooltip("A transform of a target for the enemies to \'attack\'")]
	public Transform dest_10;

	//PRIVATE VARIABLES
	private ArrayList list_enemies;
	private int e_num;
	private int num_types = 3;
	private int num_targets = 10;

	private int num_enemies_per_wave;
	private int num_enemies_defeated;

	//	CONTROL VARIABLES
	//private bool justCreated;
	[Tooltip("(DEBUG) Chooses whether waves of enemies will spawn")]
	public bool useWaveSpawns = true;
	private bool isSpawnAllowed;
	private bool isWaveComplete;
	private bool hasResumed = true;

	// VIEWING VARIABLES
	[Tooltip("(DEBUG) VIEW VARIABLE\nAllows the variable's value to be viewed during the script's execution.")]
	public int view_num_enemies;
	[Tooltip("(DEBUG) VIEW VARIABLE\nAllows the variable's value to be viewed during the script's execution.")]
	public float view_timer;
	[Tooltip("(DEBUG) VIEW VARIABLE\nAllows the variable's value to be viewed during the script's execution.")]
	public float view_wave_timer;


	/*
	 * 		Enemy Spawn Locations:
	 * 			X :	-100 < x < 100
	 * 			Y = (height / 2)
	 * 			Z :	200 < z < 300 (NEAR)
	 * 					OR
	 * 				200 < z < 400 (FAR)
	 * 
	 */
	void Start () {
		if (CONTROLLERS == null) {
			CONTROLLERS = GameObject.Find ("CONTROLLERS");
		}
		_agc = CONTROLLERS.GetComponent<ArcheryGameController> ();
		_hud = CONTROLLERS.GetComponent<HudController>();

		Random.seed = (int) Time.time;

		list_enemies = new ArrayList ();

		e_num = 0;
		//justCreated = false;
		isSpawnAllowed = true;
		isWaveComplete = false;

		if (spawn_rate < 5.0f) {
			spawn_rate = 20.0f;
		}

		timer = 0.0f;
		wave_timer = spawn_rate;
		view_wave_timer = wave_timer;

		setupForNextWave (_agc.numEnemies_wave1);
	
	}//START

	void FixedUpdate () {
		if (Input.GetKeyDown (KeyCode.H) == true) {					// HELP
			print("Key Controls:\n"
				+ "\t" + "Z: Clear all enemies" + "\n"
				+ "\t" + "F: Create 1 enemy" + "\n"
				+ "\t" + "G: Create 5 enemies" + "\n"
				+ "\t" + "H: Defeat 1 enemy" + "\n"
				+ "\t" + "X: Delete 1 random enemy" + "\n");
		}

		// IF THE GAME IS PAUSED, NO UPDATES PERFORMED
		if (_agc.getIsPaused () == true) {
			setPathfindingStatus (false);
			hasResumed = false;
			return;
		} 
		else {
			if (hasResumed == false) {
				hasResumed = true;
				setPathfindingStatus (true);
			}
		}

		if (Input.GetKeyDown (KeyCode.Z) == true) {					// CLEAR ALL ENEMIES
			destroyAllEnemies();
			isSpawnAllowed = false;
			isWaveComplete = true;
		}
		if (Input.GetKeyDown (KeyCode.X) == true) {					// DELETE 1 RANDOM ENEMY
			if (getNumberOfEnemies () > 0) {
				destroyRandomEnemy ();
			}
		}
		if (Input.GetKeyDown (KeyCode.F) == true) {					// CREATE 1 RANDOM ENEMY
			spawnEnemy ();
			//justCreated = true;
		}
		if (Input.GetKeyDown (KeyCode.G) == true) {
			spawnEnemies (5);										// CREATE 5 RANDOM ENEMIES
			//justCreated = true;
		}

		if (Input.GetKeyDown (KeyCode.K) == true) {
			if (num_enemies_defeated < num_enemies_per_wave) {
				num_enemies_defeated++;
			}
			else {
				num_enemies_defeated = num_enemies_per_wave;
			}
		}

		if (isWaveComplete == false){

			if (num_enemies_defeated == num_enemies_per_wave) {
				isSpawnAllowed = false;
				isWaveComplete = true;
				return;
			}
			
			timer += Time.deltaTime;
			if (timer >= wave_timer && isSpawnAllowed == true) {
				wave_timer += spawn_rate;
				if (useWaveSpawns == true) {
					spawnEnemies (Random.Range (3, 10 + 1));
				}
				//next_spawn_countdown = wave_timer - timer;
				view_wave_timer = wave_timer;
			}

			//This for loop iterates over all the current enemies in the level
			for (int i = 0; i < list_enemies.Count; i++) {
				GameObject e = (GameObject)list_enemies [i];		//	Get the enemy game object
				NavMeshAgent a = e.GetComponent<NavMeshAgent> ();	// 	Get the NavMeshAgent Component

				if (a.hasPath == false && a.pathPending == false) {
					safelyAssignEnemyDest (e);
				}
					
			}//for - list_enemies[e]

			checkForDeadEnemies ();

			//Reset Control Variables
			//justCreated = false;
		}// if - isWaveComplete

		//Update Viewing Varaibles
		view_num_enemies = list_enemies.Count;
		view_timer = timer;
	
	}//Method -- UPDATE

	/**
	 *		Returns the total number of enemies currently in the level
	 */
	public int getNumberOfEnemies(){
		return list_enemies.Count;
	}

	public float getSpawnCountdown(){
		return (wave_timer - timer);
	}

	public int getEnemiesRemaining(){
		return num_enemies_per_wave - num_enemies_defeated;
	}

	public int getNumEnemiesDefeated(){
		return num_enemies_defeated;
	}

	public bool getIsWaveComplete(){
		return isWaveComplete;
	}

	/**
	 *		Destroys the enemy Game Object safely
	 */
	private void destroyEnemy(GameObject e){
		GameObject.DestroyImmediate (e);
		list_enemies.Remove (e);
		num_enemies_defeated++;
		_hud.decreaseSliderValue(1);
	}

	/**
	 *		Destroys a randomly chosen enemy Game Object safely
	 */
	private void destroyRandomEnemy(){
		int v = Random.Range (0, list_enemies.Count - 1);
		GameObject victim = (GameObject)list_enemies [v];
		destroyEnemy (victim);
	}

	/**
	 * 		Destroys all enemy objects in the current level
	 * 
	 */
	private int destroyAllEnemies(){

		for (int i = 0; i < list_enemies.Count; i++) {
			GameObject e = (GameObject) list_enemies [i];		//	Get the enemy game object
			destroyEnemy(e);
		}//for - list_enemies[e]
		e_num = list_enemies.Count;

		return e_num;
	}

	/**
	 * 		Destroys all enemy objects in that have zero health
	 * 
	 */
	private void checkForDeadEnemies(){

		for (int i = 0; i < list_enemies.Count; i++) {
			GameObject e = (GameObject) list_enemies [i];		//	Get the enemy game object
			Animation anim = e.GetComponent<Animation> ();

			if (e.GetComponent<Col_enemy> ().getIsDying () == true) {
				if (anim.clip.name == "run") {
					NavMeshAgent agent = e.GetComponent<NavMeshAgent> ();
					agent.Stop ();
					anim.Play ("dead");
				}
				else {
					if (anim.isPlaying == false) {
						e.GetComponent<Col_enemy> ().setIsDead (true);
					}
				}
			}

			if (e.GetComponent<Col_enemy> ().getIsDead () == true) {
				NavMeshAgent agent = e.GetComponent<NavMeshAgent> ();
				agent.Stop ();
				anim.Stop ();
				destroyEnemy (e);
			}
		}//for - list_enemies[e]

	}

	/**
	 * 		This method sets whether or not the enemies will continue on their
	 * 	path or maintain their current position.  Primarily used to freeze enemies during
	 * 	periods where the game is paused.
	 * 			@param status whether the enemies are following their paths
	 * 				"false" - stop all enemies at their current location
	 * 				"true"  - allow enemies to continue along the paths
	 * 
	 */
	private void setPathfindingStatus(bool status){

		for (int i = 0; i < list_enemies.Count; i++) {
			GameObject e = (GameObject) list_enemies [i];		//	Get the enemy game object
			NavMeshAgent agent = e.GetComponent<NavMeshAgent> ();

			if (status == false) {
				agent.Stop ();
			} 
			else {
				agent.Resume ();
			}

		}//for - list_enemies[i]
	}

	/**
	 * 
	 * 		A helper method that calls the "spawnEnemy" method (numEnemies) times
	 * 			to create multiple enemies in a single method call
	 * 
	 */
	public void spawnEnemies(int numEnemies){
		while (numEnemies > 0) {
			spawnEnemy ();
			numEnemies--;
		}
	}

	/**
	 * 		Adds a single enemy at a random spawn points and sets it
	 * 			with a random destination
	 * 
	 */ 
	public void spawnEnemy(){

		//Check to make sure the limit of enemies for the level is not violated
		if (list_enemies.Count == MAX_ENEMIES ^ isSpawnAllowed == false) {
			return;
		}

		//	DETERMINE THE type OF ENEMY
		int type = Random.Range (1, (num_types+1));
		GameObject new_enemy;
		float y_pos = Y_OFFSET;
		float z_pos;
		if (type == 1) {
			new_enemy = GameObject.Instantiate (enemyType_1);
			y_pos = enemyType_1.transform.position.y;
			z_pos = enemyType_1.transform.position.z;
		}
		else {
			if (type == 2) {
				new_enemy = GameObject.Instantiate (enemyType_2);
				y_pos = enemyType_2.transform.position.y;
				z_pos = enemyType_2.transform.position.z;
			}
			else {
				new_enemy = GameObject.Instantiate (enemyType_3);
				y_pos = enemyType_3.transform.position.y;
				z_pos = enemyType_3.transform.position.z;
			}
		}
		new_enemy.name = "Enemy (" + (e_num+1) + ")";

		NavMeshAgent agent = new_enemy.GetComponent<NavMeshAgent> ();
		if (agent == null) {
			new_enemy.AddComponent<NavMeshAgent>();
		}

		//	SET ENEMY'S position
		float x = (float) Random.Range(-100 + SAFETY_MARGIN, 101 - SAFETY_MARGIN);
		//float z = (float) Random.Range (300 + SAFETY_MARGIN, 401 - SAFETY_MARGIN);				//Far Enemy Spawn
		//float z = (float) Random.Range (200 + SAFETY_MARGIN, 401 - SAFETY_MARGIN);			//Entire Enemy Spawn

		Vector3 enemy_spawn = new Vector3 (x, y_pos, z_pos);
		//print ("enemy_spawn: " + enemy_spawn);
		new_enemy.transform.position = Vector3.Lerp (new_enemy.transform.position, enemy_spawn, Time.deltaTime * TELEPORT);

		//	ASSIGN THE ENEMY A destination
		safelyAssignEnemyDest(new_enemy);

		//	RANDOMIZE THE ENEMY'S acceleration & speed
		float acc = (float) Random.Range (ACC_LOW, ACC_HIGH+1);
		agent.acceleration = acc;

		float speed = (float) Random.Range (SPEED_LOW, SPEED_HIGH+1);
		agent.speed = speed;

		//	INCREASE THE COUNT AND ADD IT TO THE LIST OF ENEMIES
		e_num++;
		list_enemies.Add (new_enemy);

	}//Method -- spawnEnemy

	private void safelyAssignEnemyDest(GameObject e){
		bool hasPath = assignEnemyDestination (e);
		NavMeshAgent agent = e.GetComponent<NavMeshAgent> ();
		if (hasPath == false) {
			if (agent.pathPending == true) {
				//print ("...Path is being calulated. Please Standby...");
			}
			else {
				while (hasPath == false && agent.pathPending == false) {
					hasPath = assignEnemyDestination (e);
				}
			}

		}

	}

	private bool assignEnemyDestination(GameObject enemy){

		NavMeshAgent agent = enemy.GetComponent<NavMeshAgent> ();
		if (agent == null) {
			enemy.AddComponent<NavMeshAgent>();
		}
	
		//	RANDOMIZE THE ENEMY'S destination
		int r = Random.Range (1, num_targets+1);
		bool isPathSet = false;
		switch(r){
			case 1:		{isPathSet = agent.SetDestination (dest_1.position); }
				break;
			case 2:		{isPathSet = agent.SetDestination (dest_2.position); }
				break;
			case 3:		{isPathSet = agent.SetDestination (dest_3.position); }
				break;
			case 4:		{isPathSet = agent.SetDestination (dest_4.position); }
				break;
			case 5:		{isPathSet = agent.SetDestination (dest_5.position); }
				break;
			case 6:		{isPathSet = agent.SetDestination (dest_6.position); }
				break;
			case 7:		{isPathSet = agent.SetDestination (dest_7.position); }
				break;
			case 8:		{isPathSet = agent.SetDestination (dest_8.position); }
				break;
			case 9:		{isPathSet = agent.SetDestination (dest_9.position); }
				break;
			case 10:	{isPathSet = agent.SetDestination (dest_10.position); }
				break;
			default:	{isPathSet = agent.SetDestination (dest_1.position); }
				break;
		}//switch - r
	
		return isPathSet;
	}//Method -- assignEnemyDestination

	public void clearWave(){
		int success = destroyAllEnemies ();
		int i = 2;
		while (success > 0) {
			Debug.Log ("\"destroyAllEnemies()\" failed to clear all enemies! ("+success+")");
			Debug.Log ("Retry #"+ i +" to clear all enemies...");
			success = destroyAllEnemies ();
			i++;

		}
		Debug.Log ("All enemies cleared after "+ i +" tries. CONFIRM >> ("+list_enemies.Count+")");
	}

	public void setupForNextWave(int enemiesInNewWave){

		num_enemies_per_wave = enemiesInNewWave;
		num_enemies_defeated = 0;

		_hud.initSliderValues(enemiesInNewWave);

		//RESET TIMERS
		timer = 0.0f;
		wave_timer = spawn_rate;
		view_wave_timer = wave_timer;

		startNewWave ();

	}//Method -- setupForNextWave

	private void startNewWave(){
		isWaveComplete = false;
		isSpawnAllowed = true;
	}


}//CLASS BRACKET
