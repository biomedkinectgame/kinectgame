﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AnimationStarter : MonoBehaviour {

	public float set_countdown;
	private float anim_countdown;
	private float scene_timer;

	public Animation anim;
	private bool hasPlayed;
	private bool hasCameraStopped;

	public float view_animation_countdown;
	public float view_len;
	public float view_scene_timer;

	// Use this for initialization
	void Start () {

		if (set_countdown > 5.0f) {
			anim_countdown = set_countdown;
		}
		else {
			anim_countdown = 5.0f;
		}

		set_countdown = anim_countdown;

		float len = anim.GetClip("Cam_1_intro").length;
		view_len = len;

		scene_timer = anim_countdown + len + 1.0f;
		view_scene_timer = scene_timer;

		hasPlayed = false;
		hasCameraStopped = false;
	}

	// Update is called once per frame
	void Update () {

		anim_countdown -= Time.deltaTime;
		scene_timer -= Time.deltaTime;

		if (anim_countdown < 0 && hasPlayed == false) {
			hasPlayed = true;
			anim.Play();
		}

		if (scene_timer < 0) {
			hasCameraStopped = true;
			//SceneManager.LoadScene ("Trans_intro");
			/*
				Access a new script which will tell the title overlay to appear

			*/
		}

		set_countdown = anim_countdown;
	}

	public bool getHasCameraStopped(){
		return hasCameraStopped;
	}
}
