﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {

	public float set_Timer;
	private float timer;

	public float view_timer;

	// Use this for initialization
	void Start () {

		if (set_Timer > 0) {
			timer = set_Timer;
		}
		else {
			timer = 10.0f;
		}

		view_timer = timer;
		
	}
	
	// Update is called once per frame
	void Update () {

		timer -= Time.deltaTime;

		if (timer < 0) {
			SceneManager.LoadScene ("Trans-intro");
		}
		
		view_timer = timer;
	}
}
