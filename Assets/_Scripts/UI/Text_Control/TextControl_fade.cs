﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextControl_fade : MonoBehaviour {
	//The path to the directory with all the text files
	private const string dir = "Assets\\Text_Files\\";

	//Variables to access the other script
	public Canvas canvas;
	private b_skipCutscene_9 _bSC;

	//The actual Text Object
	public Text storyText;

	//Text-Related Variables
	string caption;
	int currentScene;

	//Timer-Related Variables
	private float text_timer;
	public float timer_increment;
	private float next_step;
	bool isOver;

	//Color variables (standard)
	private Color RED = new Color(255.0f, 0.0f, 0.0f);
	//private Color ORANGE = new Color(255.0f, 31.0f, 0.0f);
	//private Color YELLOW = new Color(255.0f, 255.0f, 0.0f); 
	//private Color GREEN = new Color(0.0f, 255.0f, 0.0f); 
	private Color BLUE = new Color(0.0f, 0.0f, 255.0f); 
	//private Color PURPLE = new Color(178.0f, 0.0f, 255.0f); 
	private Color WHITE = new Color(255.0f, 255, 255.0f); 
	//private Color BLACK = new Color(0.0f, 0.0f, 0.0f); 

	//Color variables (other)
	//private Color CYAN = new Color(0.0f, 255.0f, 255.0f);
	//private Color PINK = new Color(255.0f, 97.0f, 198.0f);

	/*		Color of fade-in text
	 * 			(White --> Red)
	 * 			Invisible [0.0f] --> Opaque [255.0f]
	 */
	private Color FADER = new Color (255.0f, 0.0f, 0.0f, 0.0f);
	public float r_change = 1.0f;
	public float g_change = 1.0f;
	public float b_change = 1.0f;
	public float a_change = 1.0f;


	//viewing variable
	public int view_currentScene;
	public float view_text_timer;
	public float view_next_step;

	public float view_FADER_r;
	public float view_FADER_g;
	public float view_FADER_b;
	public float view_FADER_a;


	// Use this for initialization
	void Start () {
		_bSC = canvas.GetComponent<b_skipCutscene_9> ();

		storyText.text = "";
		storyText.color = WHITE;

		text_timer = 0.0f;
		if (timer_increment < 0) {
			timer_increment = 0.1f;
		}
		next_step = text_timer + timer_increment;

		view_text_timer = text_timer;
		view_next_step = next_step;

		currentScene = _bSC.getActiveScene ();

		changeSceneSettings (currentScene);
		isOver = false;

	}

	void FixedUpdate () {
		//Key Input to change timer increment or text color
		if (Input.GetKeyDown (KeyCode.I) == true) {
			timer_increment += 0.05f;
		}
		if (Input.GetKeyDown (KeyCode.D) == true) {
			timer_increment -= 0.05f;
		}
		if (Input.GetKeyDown (KeyCode.R) == true) {
			storyText.color = RED;
		}
		if (Input.GetKeyDown (KeyCode.B) == true) {
			storyText.color = BLUE;
		}
		if (Input.GetKeyDown (KeyCode.W) == true) {
			storyText.color = WHITE;
		}
		if (Input.GetKeyDown (KeyCode.Escape) == true) {
			Application.Quit ();
		}

		//Check update story text
		text_timer += Time.deltaTime;
		view_text_timer = text_timer;

		bool hasChanged = checkSceneChange ();
		if ( hasChanged == true) {
			changeSceneSettings (currentScene);
		}

		if (text_timer > next_step) {
			//FADER.r += r_change;
			//FADER.g -= g_change;
			//FADER.b -= b_change;
			//if (!(storyText.color.a + a_change > 255.0f)) {
				//FADER.a += a_change;
				//storyText.color = new Color(255.0f, 0.0f, 0.0f, (storyText.color.a + a_change));
				//print ("storyText.color = " + storyText.color);
				//print ("\n");
			//}
		
			next_step = text_timer + timer_increment;
			view_next_step = next_step;

			view_FADER_r = FADER.r;
			view_FADER_g = FADER.g;
			view_FADER_b = FADER.b;
			view_FADER_a = FADER.a; 

		}

		if (isOver == true){
			Application.Quit ();
		}

		//UPDATE VIEW VARIABLES
		view_currentScene = currentScene;
	}

	private void resetText(){
		storyText.text = "";
	}

	private bool checkSceneChange(){
		if (currentScene == _bSC.getActiveScene())
			return false;
		else {
			currentScene = _bSC.getActiveScene();
			return true;
		}
	}

	private void changeSceneSettings(int currentScene){

		switch (currentScene) {
		case 1:{
				//storyText.color = WHITE;
				changeSceneText("s1_text.txt");
			}
			break;
		case 2:{
				//storyText.color = CYAN;
				changeSceneText("s2_text.txt");
			}
			break;
		case 3:{
				//storyText.color = YELLOW;
				changeSceneText("s3_text.txt");
			}
			break;
		case 4:{
				//storyText.color = GREEN;
				changeSceneText("s4_text.txt");
			}
			break;
		case 5:{
				//storyText.color = BLUE;
				changeSceneText("s5_text.txt");
			}
			break;
		case 6:{
				//storyText.color = PURPLE;
				changeSceneText("s6_text.txt");
			}
			break;
		case 7:{
				//storyText.color = PINK;
				changeSceneText("s7_text.txt");
			}
			break;
		case 8:{
				//storyText.color = BLACK;
				changeSceneText("s8_text.txt");
			}
			break;
		case 9:{
				//storyText.color = RED;
				changeSceneText("s9_text.txt");
			}
			break;
		default:{
				//storyText.color = YELLOW;
				changeSceneText("default.txt");
				isOver = true;
			}
			break;
		}//switch

		//CLEAR THE TEXT
		//resetText();

	}//Method -- changeSceneSettings

	private void changeSceneText(string file){

		if (file.EndsWith (".txt") == false) {	file += ".txt";	}

		try{
			caption = System.IO.File.ReadAllText(dir + file);
		}
		catch(MissingReferenceException){
			caption = "... ... ...";
		}

		storyText.text = caption;
		print ("|storyText.text| " + storyText.text);
	}
		

}//CLASS BRACKET


