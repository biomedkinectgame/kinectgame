﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextControl_1by1 : MonoBehaviour {

	//Variables to access the other script
	public Canvas canvas;
	private b_skipCutscene_9 _bSC;		//The Default button script to use

	//The actual Text Object
	public Text storyText;

	//Text-Related Variables
	int index = 0;
	string x;
	char[] a;
	//bool hasChanged;
	int currentScene;

	//Timer-Related Variables
	private float text_timer;
	public float timer_increment;
	private float next_step;

	//Color variables (standard)
	private Color RED = new Color(255, 0, 0);
	//private Color ORANGE = new Color(255, 106, 0);
	private Color YELLOW = new Color(255, 255, 0); 
	//private Color GREEN = new Color(0, 255, 0); 
	//private Color BLUE = new Color(0, 0, 255); 
	//private Color PURPLE = new Color(178, 0, 255); 
	private Color WHITE = new Color(255, 255, 255); 
	private Color BLACK = new Color(0, 0, 0); 

	//Color variables (other)
	//private Color CYAN = new Color(0,255,255);
	private Color PINK = new Color(255,97,198); 


	//viewing variable
	public float view_text_timer;
	public float view_next_step;

	// Use this for initialization
	void Start () {
		_bSC = canvas.GetComponent<b_skipCutscene_9> ();

		storyText.text = "";

		text_timer = 0.0f;
		if (timer_increment < 0) {
			timer_increment = 1.0f;
		}
		next_step = text_timer + timer_increment;

		view_text_timer = text_timer;
		view_next_step = next_step;

		//x = System.IO.File.ReadAllText("myfile.txt");
		x = "A long time ago, in a galaxy far, far away...";
		a = x.ToCharArray ();

		//hasChanged = false;
		currentScene = _bSC.getActiveScene ();

	}

	void Update () {
		text_timer += Time.deltaTime;
		view_text_timer = text_timer;

		bool testChange = checkSceneChange ();
		if ( testChange == false) {
			changeSceneSettings (currentScene);
		}

		if (text_timer > next_step) {
			if (index < a.Length) {
				storyText.text += a [index];
				index++;
			} else {
				storyText.text += "\n";
				index = 0;
			}
			next_step = text_timer + timer_increment;

			view_next_step = next_step;

		}
	}

	private void resetText(){
		storyText.text = "";
		index = 0;
	}

	private bool checkSceneChange(){
		if (currentScene == _bSC.getActiveScene())
			return true;
		else {
			currentScene = _bSC.getActiveScene();
			return false;
		}
	}

	private void changeSceneSettings(int currentScene){

		switch (currentScene) {
			case 1:{
				storyText.color = RED;
			}
				break;
			case 2:{
				storyText.color = BLACK;
			}
				break;
			case 3:{
				storyText.color = YELLOW;
			}
				break;
			case 4:{
				storyText.color = PINK;
			}
				break;
			case 5:{
				storyText.color = WHITE;
			}
				break;
			case 6:{
				storyText.color = BLACK;
			}
				break;
			case 7:{
				storyText.color = BLACK;
			}
				break;
			case 8:{
				storyText.color = WHITE;
			}
				break;
			default:{
				storyText.color = YELLOW;
			}
				break;
		}//switch

		//CLEAR THE TEXT
		resetText();
	}

}//CLASS BRACKET


