﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/**
 * 	This version of the script "TextControl_1by1" has been modified
 * 		specifically forthe follwing scene: "Trans-intro"
 */
public class TextControl_intro : MonoBehaviour {
	//The path to the directory with all the text files
	private const string dir = "Assets\\Text_Files\\";

	//Variables to access the other script
	public Canvas canvas;
	private b_skipCutscene_9 _bSC;

	//The actual Text Object
	public Text storyText;

	//Text-Related Variables
	int index = 0;
	string x;
	string caption;
	char[] a;
	int currentScene;

	//Timer-Related Variables
	private float text_timer;
	public float timer_increment;
	private float next_step;
	bool isOver;

	//Color variables (standard)
	private Color RED = new Color(255.0f, 0.0f, 0.0f);
	//private Color ORANGE = new Color(255.0f, 31.0f, 0.0f);
	private Color YELLOW = new Color(255.0f, 255.0f, 0.0f); 
	private Color GREEN = new Color(0.0f, 255.0f, 0.0f); 
	private Color BLUE = new Color(0.0f, 0.0f, 255.0f); 
	private Color PURPLE = new Color(178.0f, 0.0f, 255.0f); 
	private Color WHITE = new Color(255.0f, 255, 255.0f); 
	private Color BLACK = new Color(0.0f, 0.0f, 0.0f); 

	//Color variables (other)
	private Color CYAN = new Color(0.0f, 255.0f, 255.0f);
	private Color PINK = new Color(255.0f, 97.0f, 198.0f); 


	//viewing variable
	public int view_currentScene;
	public float view_text_timer;
	public float view_next_step;


	// Use this for initialization
	void Start () {
		_bSC = canvas.GetComponent<b_skipCutscene_9> ();

		storyText.text = "";

		text_timer = 0.0f;
		if (timer_increment < 0) {
			timer_increment = 0.1f;
		}
		next_step = text_timer + timer_increment;

		view_text_timer = text_timer;
		view_next_step = next_step;

		caption = System.IO.File.ReadAllText(dir + "default.txt");
		a = caption.ToCharArray ();

		//x = "A long time ago, in a galaxy far, far away...";
		//a = x.ToCharArray ();

		currentScene = _bSC.getActiveScene ();

		changeSceneSettings (currentScene);
		isOver = false;

	}

	void Update () {
		//Key Input to change timer increment or text color
		if (Input.GetKeyDown (KeyCode.I) == true) {
			timer_increment += 0.05f;
		}
		if (Input.GetKeyDown (KeyCode.D) == true) {
			timer_increment -= 0.05f;
		}
		if (Input.GetKeyDown (KeyCode.R) == true) {
			storyText.color = RED;
		}
		if (Input.GetKeyDown (KeyCode.B) == true) {
			storyText.color = BLUE;
		}
		if (Input.GetKeyDown (KeyCode.Escape) == true) {
			Application.Quit ();
		}

		//Check update story text
		text_timer += Time.deltaTime;
		view_text_timer = text_timer;

		bool hasChanged = checkSceneChange ();
		if ( hasChanged == false) {
			changeSceneSettings (currentScene);
		}

		if (text_timer > next_step) {
			if (index < a.Length) {
				storyText.text += a [index];
				index++;
			} else {
				//storyText.text += "\n";
				//index = 0;
			}
			next_step = text_timer + timer_increment;

			view_next_step = next_step;

		}

		if (isOver == true){
			Application.Quit ();
		}

		//UPDATE VIEW VARIABLES
		view_currentScene = currentScene;
	}

	private void resetText(){
		storyText.text = "";
		index = 0;
	}

	private bool checkSceneChange(){
		if (currentScene == _bSC.getActiveScene())
			return true;
		else {
			currentScene = _bSC.getActiveScene();
			return false;
		}
	}

	private void changeSceneSettings(int currentScene){

		switch (currentScene) {
			case 1:{
				storyText.color = WHITE;
				changeSceneText("s1_text.txt");
			}
				break;
			case 2:{
				storyText.color = CYAN;
				changeSceneText("s2_text.txt");
			}
				break;
			case 3:{
				storyText.color = YELLOW;
				changeSceneText("s3_text.txt");
			}
				break;
			case 4:{
				storyText.color = GREEN;
				changeSceneText("s4_text.txt");
			}
				break;
			case 5:{
				storyText.color = BLUE;
				changeSceneText("s5_text.txt");
			}
				break;
			case 6:{
				storyText.color = PURPLE;
				changeSceneText("s6_text.txt");
			}
				break;
			case 7:{
				storyText.color = PINK;
				changeSceneText("s7_text.txt");
			}
				break;
			case 8:{
				storyText.color = BLACK;
				changeSceneText("s8_text.txt");
			}
				break;
			case 9:{
				storyText.color = RED;
				changeSceneText("s9_text.txt");
			}
			break;
			default:{
				storyText.color = YELLOW;
				changeSceneText("default.txt");
				isOver = true;
			}
				break;
		}//switch

		//CLEAR THE TEXT
		resetText();
	}//Method -- changeSceneSettings

	private void changeSceneText(string file){

		if (file.EndsWith (".txt") == false) {
			file += ".txt";
		}

		try{
			caption = System.IO.File.ReadAllText(dir + file);
		}
		catch(MissingReferenceException){
			caption = "... ... ...";
		}
		a = caption.ToCharArray ();
	}

}//CLASS BRACKET


