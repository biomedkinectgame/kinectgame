﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class b_skipCutscene_7 : MonoBehaviour {

	public RawImage scene_1;
	public RawImage scene_2;
	public RawImage scene_3;
	public RawImage scene_4;
	public RawImage scene_5;
	public RawImage scene_6;
	public RawImage scene_7;

	private int current = 1;

	public int getActiveScene(){
		return current;
	}

	// Use this for initialization
	void Start () {
		current = 1;
		scene_1.gameObject.SetActive (true);
		scene_2.gameObject.SetActive (false);
		scene_3.gameObject.SetActive (false);
		scene_4.gameObject.SetActive (false);
		scene_5.gameObject.SetActive (false);
		scene_6.gameObject.SetActive (false);
		scene_7.gameObject.SetActive (false);
	}
	
	public void OnClicked(){
		current++;
		changeScene (current);
	}

	private void changeScene(int active){

		switch (active){
			case 1:{
				scene_1.gameObject.SetActive (true);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
			}
				break;
			case 2:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (true);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
			}
				break;
			case 3:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (true);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
			}
				break;
			case 4:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (true);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
			}
				break;
			case 5:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (true);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);
			}
				break;
			case 6:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (true);
				scene_7.gameObject.SetActive (false);
			}
				break;
			case 7:{
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (true);
			}
				break;
			default:{
				/*Currently, it just sets all inactive.  In the game, the default case will
				be used to bring the player to the next warm-up, game, or cool down*/
				scene_1.gameObject.SetActive (false);
				scene_2.gameObject.SetActive (false);
				scene_3.gameObject.SetActive (false);
				scene_4.gameObject.SetActive (false);
				scene_5.gameObject.SetActive (false);
				scene_6.gameObject.SetActive (false);
				scene_7.gameObject.SetActive (false);

				//Load the next game
				SceneManager.LoadScene("Kick Game");
			}
				break;
		}//switch

	}//Method Bracket -- changeScene(...)

}//CLASS BRACKET


























