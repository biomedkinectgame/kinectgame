﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class b_skipCutscene_1 : MonoBehaviour {

	public RawImage scene_1;

	private int current = 1;

	public int getActiveScene(){
		return current;
	}

	// Use this for initialization
	void Start () {
		current = 1;
		scene_1.gameObject.SetActive (true);
	}
	
	public void OnClicked(){
		current++;
		changeScene (current);
	}

	private void changeScene(int active){

		switch (active){
			case 1:{
				scene_1.gameObject.SetActive (true);
			}
				break;
			default:{
				/*Currently, it just sets all inactive.  In the game, the default case will
				be used to bring the player to the next warm-up, game, or cool down*/
				scene_1.gameObject.SetActive (false);

				//Load the next game
				SceneManager.LoadScene("Kick Game");
			}
				break;
		}//switch

	}//Method Bracket -- changeScene(...)

}//CLASS BRACKET


























