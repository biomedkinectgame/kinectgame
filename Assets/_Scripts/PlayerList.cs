﻿using UnityEngine;
using System.Collections;

public class PlayerList : MonoBehaviour {
    
    public PlayerDataController playerDataController;
    public float y;
    PlayerData player;
    bool gui;
    // Use this for initialization
    void Start () {
        gui = false;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        if (gui == true)
        {
            for (int i = 0; i < playerDataController.PlayerList.Count; i++)
            {
                player = (PlayerData)playerDataController.PlayerList[i];
                if (GUI.Button(new Rect(0, 50+i * y, 300, 100 ), player.name))
                {
                    playerDataController.ChangePlayer(player);
                    OpenList();
                    gameObject.GetComponent<MenuController>().PlayerListToMainScreen();
                    playerDataController.CheckGender();
                }
            }
        }
    }

    public void OpenList()
    {
        if (gui == true)
        {
            gui = false;
        }
        else
        {
            gui = true;
        }
        
    }

    void swap(PlayerData player)
    {
        for (int i = playerDataController.PlayerList.Count - 1; i >0; i--)
        {
            playerDataController.PlayerList[i + 1] = playerDataController.PlayerList[i];
        }
    }
}

