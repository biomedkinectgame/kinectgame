﻿using UnityEngine;
using System.Collections;

public class BoatPickup : MonoBehaviour
{
    public int PointsGiven = 1;
    public GameObject FlourishPrefab;

    void OnTriggerEnter(Collider other)
    {
        // When we collide with the player
        if (other.CompareTag(Tags.Player))
        {
            // give them our points
            BoatGameController.Instance.Score += PointsGiven;

            // spawn a flourish
            SpawnPrefab.Instance.Spawn(FlourishPrefab, transform.position, Quaternion.identity);

            // despawn this
            Destroy(gameObject);
        }
    }
}
