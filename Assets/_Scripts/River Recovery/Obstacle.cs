﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public int Damage = 1;
    public bool stationary = false;

    public void OnCollideWithPlayer(BoatPlayerController p, ContactPoint[] contacts)
    {
        // TODO WIP
        // do the damage
        // mark this as non-threatening so 

        p.TakeDamage(Damage, transform);

        // knock player away
        if (stationary)
        {
            // TODO
        }
        // knock me away
        else
        {
            // TODO
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.transform.CompareTag(Tags.Player))
        {
            var p = col.transform.GetComponent<BoatPlayerController>();
            OnCollideWithPlayer(p, col.contacts);
        }
    }
}
