﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        // if it's the player
        if (other.CompareTag(Tags.Player))
        {
            // tell the game to end in a victory
            BoatGameController.Instance.GoalReached();
        }
    }
}
