﻿//using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum PlayerState
{
    Idle, Animating, Dead
}

public class BoatPlayerController : MonoBehaviour
{
    [SerializeField]
    private bool GodMode = false;

    // used for move animations
    public float thrustForce = 3.0f;
    private const float AnimTime = .8f;

    public int Health;
    public PlayerState State;

    public float Speed = .5f;

    // how long am i invincible after taking damage?
    private const float InvincibilityDuration = .7f;

    [SerializeField]
    private List<AudioClip> sfxHurt;
    [SerializeField]
    private List<AudioClip> sfxRow;
    private AudioSource _audio;

    // Use this for initialization
    void Start()
    {
        State = PlayerState.Idle;
        _audio = GetComponent<AudioSource>();
    }

    public void GoLeft()
    {
        if (State != PlayerState.Idle) return;

        GetComponent<Rigidbody>().AddForce(thrustForce * -transform.right);
        PlayRowSound();
    }

    public void GoRight()
    {
        if (State != PlayerState.Idle) return;

        GetComponent<Rigidbody>().AddForce(thrustForce * transform.right);
        PlayRowSound();
    }

    public void GoForward()
    {
        if (State != PlayerState.Idle) return;

        GetComponent<Rigidbody>().AddForce(thrustForce * transform.forward);
        PlayRowSound();
    }

    void PlayRowSound()
    {
        //_audio.Stop();
        _audio.PlayOneShot(sfxRow[Random.Range(0, sfxRow.Count)]);
    }

    public void TakeDamage(int amount, Transform source = null)
    {
        if (State == PlayerState.Dead || GodMode)
            return;

        Health--;
        var flasher = GetComponent<Flasher>();
        StartCoroutine(flasher.DoFlashes(new List<Color> { Color.red }, InvincibilityDuration));
        StartCoroutine(InvincibilityTimer());

        // Play hurt sfx
        _audio.PlayOneShot(sfxHurt[Random.Range(0, sfxHurt.Count)]);

        // Am I dead?
        if (Health <= 0)
        {
            Health = 0;
            State = PlayerState.Dead;
        }
    }

    /// <summary>
    /// Makes the player invincible for a time
    /// </summary>
    /// <returns></returns>
    IEnumerator InvincibilityTimer()
    {
        bool origMode = GodMode;
        GodMode = true;
        
        yield return new WaitForSeconds(InvincibilityDuration);

        GodMode = origMode;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag(Tags.Terrain))
        {
            TakeDamage(1);
        }
    }
}
