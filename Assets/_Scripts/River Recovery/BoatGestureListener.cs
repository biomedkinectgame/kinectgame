using UnityEngine;
using UnityEngine.UI;
//using Windows.Kinect;
using System.Collections;
using System;


public class BoatGestureListener : MonoBehaviour, KinectGestures.GestureListenerInterface
{
    [Tooltip("GUI-Text to display gesture-listener messages and gesture information.")]
    public Text GestureInfo;

    public bool row = false;

    private BoatGameController _gc;

    public void Start()
    {
        _gc = BoatGameController.Instance;
    }

    public void UserDetected(long userId, int userIndex)
    {
        // as an example - detect these user specific gestures
        KinectManager manager = KinectManager.Instance;
        manager.DetectGesture(userId, KinectGestures.Gestures.RowForward);
        manager.DetectGesture(userId, KinectGestures.Gestures.RowLeft);
        manager.DetectGesture(userId, KinectGestures.Gestures.RowRight);
        manager.DetectGesture(userId, KinectGestures.Gestures.RaiseLeftHand);

        if (GestureInfo != null)
        {
            GestureInfo.text = "To start, raise your left hand.";
        }
    }

    public void UserLost(long userId, int userIndex)
    {
        if (GestureInfo != null)
        {
            GestureInfo.text = string.Empty;
        }
    }

    public void GestureInProgress(long userId, int userIndex, KinectGestures.Gestures gesture,
                                  float progress, KinectInterop.JointType joint, Vector3 screenPos)
    {
    }

    public bool GestureCompleted(long userId, int userIndex, KinectGestures.Gestures gesture,
                                 KinectInterop.JointType joint, Vector3 screenPos)
    {
        // Start game by raising left hand
        if (_gc.State == BoatGameController.GameState.Pregame &&
            gesture == KinectGestures.Gestures.RaiseLeftHand)
        {
            row = true;
            _gc.StartGame();
        }
        // While game running
        else if (_gc.State == BoatGameController.GameState.Running && row)
        {
            // Forward
            switch (gesture)
            {
                case KinectGestures.Gestures.RowForward:
                    _gc.HandleForward();
                    break;
                case KinectGestures.Gestures.RowLeft:
                    _gc.HandleLeft();
                    break;
                case KinectGestures.Gestures.RowRight:
                    _gc.HandleRight();
                    break;
            }
        }
        else if (_gc.State == BoatGameController.GameState.EndLose &&
            gesture == KinectGestures.Gestures.RaiseLeftHand)
        {
            _gc.RestartGame();
        }
        // @David -- this is where it's set to go to the next level if the player raise their left hand after they win -KR
        else if (_gc.State == BoatGameController.GameState.EndWin &&
            gesture == KinectGestures.Gestures.RaiseLeftHand)
        {
            _gc.GoToNextLevel();
        }

        return true;
    }

    public bool GestureCancelled(long userId, int userIndex, KinectGestures.Gestures gesture,
                                 KinectInterop.JointType joint)
    {
        if (GestureInfo != null)
        {
            GestureInfo.text = String.Empty;
        }

        return true;
    }

    public void Update()
    {

        if (GestureInfo != null)
        {
            GestureInfo.text = String.Empty;
        }
    }
}
