﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Hover : MonoBehaviour
{
    public float amplitude;
    public float frequency;
    
    public Vector3 axis;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position += amplitude * (Mathf.Sin(2 * Mathf.PI * frequency * Time.time) - Mathf.Sin(2 * Mathf.PI * frequency * (Time.time - Time.deltaTime))) * axis.normalized;
    }
}
