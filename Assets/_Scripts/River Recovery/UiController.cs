﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Manages the UI elements in the scene
/// </summary>
public class UiController : MonoBehaviour
{
    private static UiController _instance;
    public static UiController Instance
    {
        get { return _instance; }
    }

    public RectTransform PregamePanel;
    public RectTransform GameOverPanel;
    public RectTransform VictoryPanel;

    public Text ScoreDisplay;
    public Text GameOverScoreDisplay;
    public Text VictoryScoreDisplay;
    public Slider HealthSlider;
    //public Text HealthDisplay;
    private int _maxHp;

    void Update()
    {
        ScoreDisplay.text = BoatGameController.Instance.Score.ToString();
        //HealthDisplay.text = BoatGameController.Instance.Player.Health.ToString();
        HealthSlider.value = (float)BoatGameController.Instance.Player.Health / (float)_maxHp;
    }

    void Awake()
    {
        _instance = this;
        PregamePanel.gameObject.SetActive(true);
    }

    void Start()
    {
        _maxHp = BoatGameController.Instance.Player.Health;
    }

    public void HidePregamePanel()
    {
        // todo: UI animations would probably be much cuter
        PregamePanel.gameObject.SetActive(false);
    }

    public void ShowGameOverPanel()
    {
        GameOverScoreDisplay.text = "Score: " + BoatGameController.Instance.Score.ToString();
        GameOverPanel.gameObject.SetActive(true);
    }

    public void ShowVictoryPanel()
    {
        VictoryScoreDisplay.text = "Score: " + BoatGameController.Instance.Score.ToString();
        VictoryPanel.gameObject.SetActive(true);
    }
}
