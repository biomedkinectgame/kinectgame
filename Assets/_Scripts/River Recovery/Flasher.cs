﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Flasher : MonoBehaviour
{
    public IEnumerator DoFlashes(ICollection<Color> colors, float animTime)
    {
        var origColors = new Dictionary<Renderer, Color>();
        var elapsedTime = 0f;
        var rens = GetComponentsInChildren<Renderer>();
        foreach (var r in rens)
        {
            origColors.Add(r, r.material.color);
        }
        var doWhite = false;

        while (elapsedTime < animTime)
        {
            var col = doWhite ? colors.ElementAt(Random.Range(0, colors.Count)) : Color.white;

            foreach (var r in rens)
            {
                r.material.color = col;
            }

            doWhite = !doWhite;
            elapsedTime += Time.smoothDeltaTime;
            yield return null;
        }
        // make sure it goes back to default
        foreach (var entry in origColors)
        {
            entry.Key.material.color = entry.Value;
        }
    }
}
