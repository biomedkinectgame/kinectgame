﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTimeout : MonoBehaviour
{
    public float Timeout = 1f;
    public float DestroyTime = 0f;

    // Use this for initialization
    void Start()
    {
        DestroyTime = Time.time + Timeout;
    }

    // Update is called once per frame
    void Update()
    {
        if (DestroyTime < Time.time)
        {
            Destroy(this.gameObject);
        }
    }
}
