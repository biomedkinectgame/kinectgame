﻿using UnityEngine;
using System.Collections;

public class DanceDemoController : MonoBehaviour {

    public GUIText Score;
    public GUIText Test;
    public GameObject danceBox;
    public GameObject hand1;
    public GameObject hand2;

    public float produceRate;

    public int score;
    float produceTime;

    Vector3 boundryA;
    Vector3 boundryB;

    Vector3 center;
    float radius;

    Vector3 position;
    float x;
    float y;

    bool gameStarted;
	// Use this for initialization
	void Start () {
        produceTime = 0;
        score = 0;
	}
	
	// Update is called once per frame
	void Update () {
        Score.text = "Score:" + score;
        if (Time.time >= produceTime&&gameStarted==true)
        {
            produceTime = Time.time + produceRate;
            x = Random.Range(center.x - radius, center.x + radius);
            y = Random.Range(center.y - Mathf.Sqrt(radius * radius - Mathf.Pow(Mathf.Abs(center.x - x), 2)), center.y + Mathf.Sqrt(radius * radius - Mathf.Pow(Mathf.Abs(center.x - x), 2)));
            position = new Vector3(x, y, -7);
            Instantiate(danceBox, position, danceBox.transform.rotation);
        }
	}

    public void GameStart()
    {
        if (gameStarted == true)
        {
            gameStarted = false;
        }
        else
        {
            gameStarted = true;
        }
        
    }

    public void  Boundry()
    {
        StartCoroutine(MakeBoundry());
    }

    IEnumerator MakeBoundry()
    {
        Test.text = "Position and Reach test in 3 seconds";
        yield return new WaitForSeconds(1);
        Test.text = "Position and Reach test in 2 seconds";
        yield return new WaitForSeconds(1);
        Test.text = "Position and Reach test in 1 seconds";
        yield return new WaitForSeconds(1);
        Test.text = "";
        Vector3 a = hand1.transform.position;
        Vector3 b = hand2.transform.position;

        center = new Vector3((a.x + b.x) / 2, (a.y + b.y) / 2, -7);
        radius = Mathf.Abs((a.x - b.x) / 2);

        Debug.Log(center+""+radius);
    }
}
