﻿using UnityEngine;
using System.Collections;

public class Cube : MonoBehaviour {

    private int colliderNumber;
	// Use this for initialization
	void Start () {
        colliderNumber = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (colliderNumber == 0)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.white;
        }

        if (colliderNumber == 1)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }

        if (colliderNumber == 2)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.blue;
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hand")
        {
            colliderNumber++;
            
            
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Hand")
        {
            colliderNumber--;

        }
    }
}
