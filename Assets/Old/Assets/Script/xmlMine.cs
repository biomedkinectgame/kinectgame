﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
public class xmlMine : MonoBehaviour {

    string fileLocation, fileName;
    public GameObject player;
    UserData myData;
    string testName;
    string data;

    bool testing;

    StreamWriter writer;
    FileInfo t;

    Vector3 playerPosition;
        
	// Use this for initialization
	void Start () {
        fileLocation = Application.dataPath;
        fileName = "PositionData.xml";

        testName = "Test1";

        
        t = new FileInfo(fileLocation + "\\" + fileName);
        writer = t.CreateText();
        myData = new UserData();
	}
	
	// Update is called once per frame
	void Update () {
        if (testing == true)
        {
            myData.positionData.name = testName;
            myData.positionData.x = player.transform.position.x;
            myData.positionData.y = player.transform.position.y;
            myData.positionData.z = player.transform.position.z;
            data = SerializeObject(myData);
            WrieteXML();
        }
	
	}

    public void SaveData()
    {
        if (testing == true)
        {
            testing = false;
        }
        else
        {
            
        testing = true;
        }
    }

    void WrieteXML()
    {
        
        writer.Write(data);
        writer.Close();
    }


    string UTF8ByteArrayToString(byte[] characters)
    {
        UTF8Encoding encoding = new UTF8Encoding();
        string constructedString = encoding.GetString(characters);
        return (constructedString);
    }

    string SerializeObject(object pObject)
    {
        string XmlizedString = null;
        MemoryStream memoryStream = new MemoryStream();
        XmlSerializer xs = new XmlSerializer(typeof(UserData));
        XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
        xs.Serialize(xmlTextWriter, pObject);
        memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
        XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
        return XmlizedString;
    }

    public class UserData
    {
        public PositionData positionData;
        public UserData() { }
        public struct PositionData
        {
            public double x, y, z;
            public string name;
        }
    }
}
