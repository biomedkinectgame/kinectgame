﻿using UnityEngine;
using System.Collections;

public class MyMenuController : MonoBehaviour {

    public GameObject LeftHand;
    public GameObject RightHand;
    public GameObject Shoulder;
    public GameObject hip;
    public GameObject LeftElbow;
    public GameObject RightElbow;

    public GameObject LeftTopCube, RightTopCube, LeftBottomCube, RightBottomCube;

    bool leftTopSelected, leftBottomSelected, rightTopSelected, rightBottomSelected;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (LeftHand.transform.position.x < LeftElbow.transform.position.x && LeftHand.transform.position.y > Shoulder.transform.position.y && LeftHand.transform.position.z > hip.transform.position.z)
        {
            leftTopSelected = true;
            if (LeftTopCube.transform.position.z > -1)
            {
                MoveCube(LeftTopCube);
            }
            else
            {
                LeftTopCube.transform.position = new Vector3(LeftTopCube.transform.position.x, LeftTopCube.transform.position.y, -1);
            }
        }
        else
        {
            leftTopSelected = false;
            if (LeftTopCube.transform.position.z < 0)
            {
                MoveBack(LeftTopCube);
            }
            else
            {
                LeftTopCube.transform.position = new Vector3(LeftTopCube.transform.position.x, LeftTopCube.transform.position.y, 0);
            }
        }

        if (RightHand.transform.position.x > RightElbow.transform.position.x && RightHand.transform.position.y > Shoulder.transform.position.y && RightHand.transform.position.z > hip.transform.position.z)
        {
            rightTopSelected = true;
            if (RightTopCube.transform.position.z > -1)
            {
                MoveCube(RightTopCube);
            }
            else
            {
                RightTopCube.transform.position = new Vector3(RightTopCube.transform.position.x, RightTopCube.transform.position.y, -1);
            }
        }
        else
        {
            rightTopSelected=false;
            if (RightTopCube.transform.position.z < 0)
            {
                MoveBack(RightTopCube);
            }
            else
            {
                RightTopCube.transform.position = new Vector3(RightTopCube.transform.position.x, RightTopCube.transform.position.y, 0);
            }
        }


        if (LeftHand.transform.position.x < LeftElbow.transform.position.x && LeftHand.transform.position.y < Shoulder.transform.position.y && LeftHand.transform.position.z > hip.transform.position.z)
        {
            leftBottomSelected = true;
            if (LeftBottomCube.transform.position.z > -1)
            {
                MoveCube(LeftBottomCube);
            }
            else
            {
                LeftBottomCube.transform.position = new Vector3(LeftBottomCube.transform.position.x, LeftBottomCube.transform.position.y, -1);
            }
        }
        else
        {
            leftBottomSelected = false;
            if (LeftBottomCube.transform.position.z < 0)
            {
                MoveBack(LeftBottomCube);
            }
            else
            {
                LeftBottomCube.transform.position = new Vector3(LeftBottomCube.transform.position.x, LeftBottomCube.transform.position.y, 0);
            }
        }


        if (RightHand.transform.position.x > RightElbow.transform.position.x && RightHand.transform.position.y < Shoulder.transform.position.y && RightHand.transform.position.z > hip.transform.position.z)
        {
            rightBottomSelected = true;
            if (RightBottomCube.transform.position.z > -1)
            {
                MoveCube(RightBottomCube);
            }
            else
            {
                RightBottomCube.transform.position = new Vector3(RightBottomCube.transform.position.x, RightBottomCube.transform.position.y, -1);
            }
        }
        else
        {
            rightBottomSelected = false;
            if (RightBottomCube.transform.position.z < 0)
            {
                MoveBack(RightBottomCube);
            }
            else
            {
                RightBottomCube.transform.position = new Vector3(RightBottomCube.transform.position.x, RightBottomCube.transform.position.y, 0);
            }
        }

        if (LeftTopCube.transform.position.z < 0 && LeftHand.transform.position.x < LeftElbow.transform.position.x && LeftHand.transform.position.y > Shoulder.transform.position.y)
        {
            if (LeftHand.transform.position.z < hip.transform.position.z)
            {
                Debug.Log("TopLeft");
                Application.LoadLevel("CubeDemo");
            }
        }

        if (RightTopCube.transform.position.z < 0 && RightHand.transform.position.x > RightElbow.transform.position.x && RightHand.transform.position.y > Shoulder.transform.position.y)
        {
            if (RightHand.transform.position.z < hip.transform.position.z)
            {
                Application.LoadLevel("TestDemo");
            }
        }

        if (LeftBottomCube.transform.position.z < 0 && LeftHand.transform.position.x < LeftElbow.transform.position.x && LeftHand.transform.position.y < Shoulder.transform.position.y)
        {
            if (LeftHand.transform.position.z < hip.transform.position.z)
            {
                Application.LoadLevel("ropeSkipping");
            }
        }

        if (RightBottomCube.transform.position.z < 0 && RightHand.transform.position.x > RightElbow.transform.position.x && RightHand.transform.position.y < Shoulder.transform.position.y)
        {
            if (RightHand.transform.position.z < hip.transform.position.z)
            {
                Debug.Log("BottomRight");
                Application.LoadLevel("DanceDemo");
            }
        }
	
	}

    void MoveCube(GameObject cube)
    {
        Vector3 direction = new Vector3(0, 0, -3);
        cube.GetComponent<Rigidbody>().velocity = direction;
    }

    void MoveBack(GameObject cube)
    {
        Vector3 direction = new Vector3(0, 0, 1);
        cube.GetComponent<Rigidbody>().velocity = direction;
    }

}
