﻿using UnityEngine;
using System.Collections;

public class WallStats : MonoBehaviour {

    public GUIText WallHP;
    public float HP;


	// Use this for initialization
	void Start () {
        HP = 100;
	
	}
	
	// Update is called once per frame
	void Update () {
        WallHP.text = "HP:" + HP;
        if (HP <= 0)
        {
            Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider other)
    {
    }
}
