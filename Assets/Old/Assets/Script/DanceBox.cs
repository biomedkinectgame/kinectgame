﻿using UnityEngine;
using System.Collections;

public class DanceBox : MonoBehaviour {

    GameObject gameController;
	// Use this for initialization
	void Start () {
        gameController = GameObject.FindGameObjectWithTag("GameController");
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hand")
        {
           Destroy(gameObject);
           gameController.GetComponent<DanceDemoController>().score++;
        }
    }
}
