﻿using UnityEngine;
using System.Collections;

public class StepBoxController : MonoBehaviour {

    float rng;
    float oldrng;
    double time;
    public GameObject topLeftBox, topRightBox, botLeftBox, botRightBox;
    public float frequency;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        if (oldrng == 0 && topLeftBox.GetComponent<StepBox>().colliderNumber == 2)
        {
            topLeftBox.GetComponent<Renderer>().material.color = Color.blue;
        }
        if (oldrng == 1 && topRightBox.GetComponent<StepBox>().colliderNumber == 2)
        {
            topRightBox.GetComponent<Renderer>().material.color = Color.blue;
        }
        if (oldrng == 2 && botLeftBox.GetComponent<StepBox>().colliderNumber == 2)
        {
            botLeftBox.GetComponent<Renderer>().material.color = Color.blue;
        }
        if (oldrng == 3 && botRightBox.GetComponent<StepBox>().colliderNumber == 2)
        {
            botRightBox.GetComponent<Renderer>().material.color = Color.blue;
        }
        if (oldrng == 4 && topLeftBox.GetComponent<StepBox>().colliderNumber == 1 && topRightBox.GetComponent<StepBox>().colliderNumber == 1)
        {
            topLeftBox.GetComponent<Renderer>().material.color = Color.blue;
            topRightBox.GetComponent<Renderer>().material.color = Color.blue;
        }
        if (oldrng == 5 && topRightBox.GetComponent<StepBox>().colliderNumber == 1 && botRightBox.GetComponent<StepBox>().colliderNumber == 1)
        {
            topRightBox.GetComponent<Renderer>().material.color = Color.blue;
            botRightBox.GetComponent<Renderer>().material.color = Color.blue;
        }
        if (oldrng == 6 && botRightBox.GetComponent<StepBox>().colliderNumber == 1 && botLeftBox.GetComponent<StepBox>().colliderNumber == 1)
        {
            botRightBox.GetComponent<Renderer>().material.color = Color.blue;
            botLeftBox.GetComponent<Renderer>().material.color = Color.blue;
        }
        if (oldrng == 7 && topLeftBox.GetComponent<StepBox>().colliderNumber == 1 && botLeftBox.GetComponent<StepBox>().colliderNumber == 1)
        {
            topLeftBox.GetComponent<Renderer>().material.color = Color.blue;
            botLeftBox.GetComponent<Renderer>().material.color = Color.blue;
        }
        if (oldrng == 8 && topLeftBox.GetComponent<StepBox>().colliderNumber == 1 && botRightBox.GetComponent<StepBox>().colliderNumber == 1)
        {
            topLeftBox.GetComponent<Renderer>().material.color = Color.blue;
            botRightBox.GetComponent<Renderer>().material.color = Color.blue;
        }
        if (oldrng == 9 && topRightBox.GetComponent<StepBox>().colliderNumber == 1 && botLeftBox.GetComponent<StepBox>().colliderNumber == 1)
        {
            topRightBox.GetComponent<Renderer>().material.color = Color.blue;
            botLeftBox.GetComponent<Renderer>().material.color = Color.blue;
        }

        if (time < Time.time)
        {
            topLeftBox.GetComponent<Renderer>().material.color = Color.green;
            topRightBox.GetComponent<Renderer>().material.color = Color.green;
            botLeftBox.GetComponent<Renderer>().material.color = Color.green;
            botRightBox.GetComponent<Renderer>().material.color = Color.green;
            Roll();
        }
	}

    void Roll()
    {
        time = Time.time+frequency;
        rng = Random.Range(0, 7);
        if (rng >= 0 && rng < 1 && oldrng != 0)
        {
            topLeftBox.GetComponent<Renderer>().material.color = Color.red;
            oldrng = 0;
        }
        else if (rng >= 1 && rng < 2 && oldrng != 1)
        {
            topRightBox.GetComponent<Renderer>().material.color = Color.red;
            oldrng = 1;
        }
        else if (rng >= 2 && rng < 3 && oldrng != 2)
        {
            botLeftBox.GetComponent<Renderer>().material.color = Color.red;
            oldrng = 2;
        }
        else if (rng >= 3 && rng < 4 && oldrng != 3)
        {
            botRightBox.GetComponent<Renderer>().material.color = Color.red;
            oldrng = 3;
        }
        else if (rng >= 4 && rng < 4.5 && oldrng != 4)
        {
            topLeftBox.GetComponent<Renderer>().material.color = Color.red;
            topRightBox.GetComponent<Renderer>().material.color = Color.red;
            oldrng = 4;
        }
        else if (rng >= 4.5 && rng < 5 && oldrng != 5)
        {
            topRightBox.GetComponent<Renderer>().material.color = Color.red;
            botRightBox.GetComponent<Renderer>().material.color = Color.red;
            oldrng = 5;
        }
        else if (rng >= 5 && rng < 5.5 && oldrng != 6)
        {
            botLeftBox.GetComponent<Renderer>().material.color = Color.red;
            botRightBox.GetComponent<Renderer>().material.color = Color.red;
            oldrng = 6;
        }
        else if (rng >= 5.5 && rng < 6 && oldrng != 7)
        {
            topLeftBox.GetComponent<Renderer>().material.color = Color.red;
            botLeftBox.GetComponent<Renderer>().material.color = Color.red;
            oldrng = 7;
        }
        else if (rng >= 6 && rng < 6.5 && oldrng != 8)
        {
            topLeftBox.GetComponent<Renderer>().material.color = Color.red;
            botRightBox.GetComponent<Renderer>().material.color = Color.red;
            oldrng = 8;
        }
        else if (rng >= 6.5 && rng < 7 && oldrng != 9)
        {
            topRightBox.GetComponent<Renderer>().material.color = Color.red;
            botLeftBox.GetComponent<Renderer>().material.color = Color.red;
            oldrng = 9;
        }

        else
        {
            Roll();
        }
    }
}
