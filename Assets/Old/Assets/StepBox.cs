﻿using UnityEngine;
using System.Collections;

public class StepBox : MonoBehaviour {
    public int colliderNumber;
	// Use this for initialization
	void Start () {
        colliderNumber = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (colliderNumber == 2)
        {
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Foot")
        {
            colliderNumber++;


        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Foot")
        {
            colliderNumber--;

        }
    }
}
